# LdapTokenAuthenticate

LDAP authentification for registering or replace token

## Documentation

Set LDAP configuration on plugin settings. You can set the default settings for survey too.

## Support

If you need support on configuration and installation of this plugin : [create a support ticket on support.sondages.pro](https://support.sondages.pro/).

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab]( https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2022 Denis Chenu <https://sondages.pro>
- Copyright © 2022 OECD <https://oecd.org>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/) 
