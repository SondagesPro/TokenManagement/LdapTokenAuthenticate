<?php

/**
 * LDAP authentification for registering or replace token
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021-2022 Denis Chenu <http://www.sondages.pro>
 * @copyright 2021-2022 OECD <http://www.oecd.org>
 * @license AGPL v3
 * @version 0.6.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class LdapTokenAuthenticate extends PluginBase
{

    protected $storage = 'DbStorage';

    protected static $description = 'LDAP authentification for registering or replace token.';
    protected static $name = 'LdapTokenAuthenticate';


    protected $settings = array(
        'server' => array(
            'type' => 'string',
            'label' => 'LDAP server',
            'help' => 'e.g. ldap://ldap.example.com or ldaps://ldap.example.com'
        ),
        'ldapport' => array(
            'type' => 'string',
            'label' => 'Port number',
            'help' => 'Default when omitted is 389',
        ),
        'ldapversion' => array(
            'type' => 'select',
            'label' => 'LDAP version',
            'options' => array('2' => 'LDAPv2', '3'  => 'LDAPv3'),
            'default' => '2',
        ),
        'ldapoptreferrals' => array(
            'type' => 'boolean',
            'label' => 'Select true if referrals must be followed (use false for ActiveDirectory)',
            'default' => '0'
        ),
        'ldaptls' => array(
            'type' => 'boolean',
            'help' => 'Check to enable Start-TLS encryption, when using LDAPv3',
            'label' => 'Enable Start-TLS',
            'default' => '0'
            ),
        'usersearchbase' => array(
            'type' => 'text',
            'label' => 'Base DN for the user search operation (Search and bind)',
            'help' => 'Multiple bases may be separated by a new line',
        ),
        'extrauserfilter' => array(
            'type' => 'string',
            'label' => 'Optional extra LDAP filter to be ANDed to the basic (searchuserattribute=username) filter',
            'help' => 'Don\'t forget the outmost enclosing parentheses',
        ),
        'binddn' => array(
            'type' => 'string',
            'label' => 'DN of the LDAP account used to search for the end-user\'s DN.',
            'help' => 'Optional , an anonymous bind is performed if empty',

        ),
        'bindpwd' => array(
            'type' => 'password',
            'label' => 'Password of the LDAP account used to search for the end-user\'s DN.'
        ),
        'searchuserattribute' => array(
            'type' => 'string',
            'label' => 'Attribute to compare to the given login can be uid, cn, mail, ...'
        ),
        'loginattribute' => array(
            'type' => 'select',
            'label' => 'Attribute to save login id',
            'options' => array(
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'email' => 'email',
                'participant_id' => 'participant_id',
                'attribute' => 'Attribute by description',
            ),
            'default' => 'email'
        ),
        'checkunicity' => array(
            'type' => 'boolean',
            'label' => 'Check unicity of entrie on LDAP search',
            'help' => 'After authentication done, system can search if there are a single entrie by security. If you are sure of your LDAP configuration, you can disable this test.',
            'default' => 1
        ),
        'descriptionloginattribute' => array(
            'type' => 'string',
            'label' => 'Description for extra attribute',
            'default' => '',
            'help' => "Set description on any attribute here if you select attribute by description'. If the attribute don't exist, this disable usage of LDAP access even if forced."
        ),
        //~ 'groupsearchbase' => array(
            //~ 'type' => 'string',
            //~ 'label' => 'Optional base DN for group restriction',
            //~ 'help' => 'E.g., ou=Groups,dc=example,dc=com'
        //~ ),
        //~ 'groupsearchfilter' => array(
            //~ 'type' => 'string',
            //~ 'label' => 'Optional filter for group restriction',
            //~ 'help' => 'Required if group search base set. E.g. (&(cn=limesurvey)(memberUid=$username)) or (&(cn=limesurvey)(member=$userdn))'
        //~ ),
        'mailattribute' => array(
            'type' => 'string',
            'label' => 'LDAP attribute of email address',
        ),
        'firstnameattribute' => array(
            'type' => 'string',
            'label' => 'LDAP attribute of first name'
        ),
        'lastnameattribute' => array(
            'type' => 'string',
            'label' => 'LDAP attribute of last name'
        ),
        'otherattribute' => array(
            'type' => 'text',
            'label' => 'Other attribute to return (and allow update in token table)',
            'help' => 'One attribute by line. reserved word are mail, firstname, lastname and authentication'
        ),
        'active' => array(
            'type' => 'boolean',
            'label' => 'Allow LDAP register or connect by default',
            'default' => 0,
        ),
        'forced' => array(
            'type' => 'select',
            'options' => array(
                'never' => "Never",
                'auto' => "Automatic",
                'always' => "Always",
            ),
            'label' => 'Force LDAP register or connect by default',
            'help' => 'It must be active to be forced.',
            'default' => 'auto',
        ),
        'adminaccess' => array(
            'type' => 'boolean',
            'label' => 'Disable LDAP authentication for admin user with rights on survey.',
            'help' => 'Permission checked are responses edit and tokens read',
            'default' => 1,
        ),
        'KeepLdapUser' => array(
            'type' => 'boolean',
            'label' => 'Allow usage of same LDAP authentication in different surveys',
            'help' => 'Only with global login attribute as default',
            'default' => 1,
        ),
        'ClearAllLogoutLdapUser' => array(
            'type' => 'boolean',
            'label' => 'Clear all action reset current LDAP user',
            'help' => 'When user clear all : this log out of current LDAP user in all surveys using same LDAP authentication.',
            'default' => 1,
        ),
        'NewtestLogoutLdapUser' => array(
            'type' => 'boolean',
            'label' => 'Newtest action reset current LDAP user',
            'help' => 'WThis log out of current LDAP user in all surveys using same LDAP authentication if newtest is set when start survey.',
            'default' => 0,
        ),
        'LdapIntroduction' => array(
            'type' => 'string',
            'label' => 'LDAP introduction',
            'default' => 'Please enter your username and password to enter the survey.',
        ),
        'LdapDescription' => array(
            'type' => 'html',
            'label' => 'LDAP description',
            'default' => '',
        ),
    );

    /** @inheritdoc */
    public function init()
    {
        /* Add settings update */
        $this->subscribe('beforeToolsMenuRender');
        //
        /* Update twig file */
        //$this->subscribe('getValidScreenFiles');
        /* Show form if needed */
        $this->subscribe('beforeControllerAction');
    }

    /**
     * The settings function
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings(int $surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }

        $language = $oSurvey->language;
        $aSurveyLanguage = $oSurvey->getAllLanguages();
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['lang'] = array(
            'LDAP Authentication settings' => $this->gT("LDAP Authentication settings"),
            'Introduction' => $this->gT("Introduction"),
            'Introduction text in %s (%s)' => $this->gT("Introduction text in %s (%s)"),
            'Default introduction' => $this->gT("Default introduction"),
            'Description' => $this->gT("Description"),
            'Description text in %s (%s)' => $this->gT("Description text in %s (%s)"),
            'Default description' => $this->gT("Default description"),
        );
        $aData['errors'] = array();

        /* @var array[] aSettings for SettingsWidget */
        $aSettings = array();

        /* Token attribute */
        $aTokens = array(
            'firstname' => gT("First name"),
            'lastname' => gT("Last name"),
            'email' => gT("Email"),
            'participant_id' => gT("Participant ID"),
        );
        foreach ($oSurvey->getTokenAttributes() as $attribute => $information) {
            $aTokens[$attribute] = empty($information['description']) ? $attribute : $information['description'];
        }
        $helpLdapAttribute = "";
        $defaultAttribute = $this->get("loginattribute", null, null, null);
        $emptyAttributeString = $this->gt("Leave default (mail)");
        if ($defaultAttribute) {
            $emptyAttributeString = sprintf($this->gt("Leave default : %s"), $defaultAttribute);
            if ($defaultAttribute == "attribute") {
                $descriptionAttribute = $this->get("descriptionloginattribute", null, null, null);
                if (empty($descriptionAttribute)) {
                    $emptyAttributeString = $this->gT("No default set");
                } else {
                    $attribute = $this->getUidAttribute($surveyId);
                    if (empty($attribute)) {
                        $emptyAttributeString = sprintf($this->gT("Atttribute %s didn't exist in this survey."), CHtml::encode($descriptionAttribute));
                    } else {
                        $emptyAttributeString = sprintf($this->gT("Leave default (%s)."), CHtml::encode($descriptionAttribute));
                    }
                }
            }
        }

        $activeDefault = $this->get('active', null, null, $this->settings['active']['default']) ? gT('Yes') : gT('No');
        $forcedDefault = $this->get('forced', null, null, $this->settings['forced']['default']);
        $allowRegister = $oSurvey->getIsAllowRegister() ? gT('Yes') : gT('No');
        switch ($forcedDefault) {
            case 'never':
                $forcedDefault = $this->gT("Never");
                break;
            case 'auto':
                $forcedDefault = $this->gT("Automatic");
                break;
            case 'always':
                $forcedDefault = $this->gT("Always");
                break;
        }
        $aLdapSettings = array(
            'active' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"), $activeDefault)),
                ),
                'label' => $this->gT('Use LDAP authenticate'),
                'current' => $this->get('active', 'Survey', $surveyId, '')
            ),
            'forced' => array(
                'type' => 'select',
                'options' => array(
                    'never' => $this->gT("Never"),
                    'auto' => $this->gT("Automatic"),
                    'always' => $this->gT("Always"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"), $forcedDefault)),
                ),
                'label' => $this->gT('Force LDAP authenticate'),
                'current' => $this->get('forced', 'Survey', $surveyId, ''),
                'help' => $this->gT("Always totally disable usage of single token, only allowed to enter in survey after LDAP authentication. If token is allowed : it can come from url and a the form show an password input. Automatic check if attribute used for LDAP is empty or not."),
            ),
            'create' => array(
                'type' => 'select',
                'options' => array(
                    'Y' => $this->gT("Yes"),
                    'N' => $this->gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->gT("Use allow register (%s)"), $allowRegister)),
                ),
                'label' => $this->gT('Allow creation of token by LDAP account.'),
                'current' => $this->get('create', 'Survey', $surveyId, ''),
            ),
            'loginattribute' => array(
                'type' => 'select',
                'options' => $aTokens,
                'htmlOptions' => array(
                    'empty' => $emptyAttributeString
                ),
                'label' => $this->gT('Attribute to be used to save login'),
                'current' => $this->get('loginattribute', 'Survey', $surveyId, '')
            )
        );
        $aSettings[$this->gT("LDAP authentication")] = $aLdapSettings;

        $aLdapIntroduction = array();
        $contentLanguage = "";
        $aLdapIntroductions = $this->get('LdapIntroductions', 'Survey', $surveyId, array());
        $aLdapDescriptions = $this->get('LdapDescriptions', 'Survey', $surveyId, array());
        $sLdapDefault = array(
            "introduction" => $this->get('LdapIntroduction', null, null, $this->settings['LdapIntroduction']['default']),
            "description" =>  $this->get('LdapDescription', null, null, $this->settings['LdapDescription']['default']),
        );
        $aData['sLdapDefault'] = $sLdapDefault;
        //$contentLanguage .= $this->renderPartial('admin.DefaultLanguageSetting', $aData, true);
        foreach ($aSurveyLanguage as $languageCode) {
            $sLdapIntroduction = isset($aLdapIntroductions[$languageCode]) ? trim($aLdapIntroductions[$languageCode]) : "";
            $aIntroductionContentData = array(
                'name' => "LdapIntroductions_{$languageCode}",
                'placeholder' => $sLdapDefault["introduction"],
                'value' => $sLdapIntroduction,
                'languagecode' => $languageCode,
                'languageDetail' => getLanguageDetails($languageCode),
            );
            $contentLanguage .= $this->renderPartial('admin.IntroductionSetting', array_merge($aData, $aIntroductionContentData), true);
            $sLdapDescription = isset($aLdapDescriptions[$languageCode]) ? trim($aLdapDescriptions[$languageCode]) : "";
            $aDescriptionContentData = array(
                'name' => "LdapDescriptions_{$languageCode}",
                'placeholder' => $sLdapDefault["description"],
                'value' => $sLdapDescription,
                'languagecode' => $languageCode,
                'languageDetail' => getLanguageDetails($languageCode),
            );
            $contentLanguage .= $this->renderPartial('admin.DescriptionSetting', array_merge($aData, $aDescriptionContentData), true);
        }
        $aSettings[$this->gT("Authentication LDAP introduction and description")] = array(
            'LdapLanguage' => array(
                'type' => 'info',
                'content' => $contentLanguage,
            ),
        );

        $aData['aSettings'] = $aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this), 'method' => 'actionSaveSettings', 'surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this), 'method' => 'actionSettings', 'surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId)),
        );
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }

    /**
     * The save setting function
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings(int $surveyId)
    {
        if (empty(App()->getRequest()->getPost('save' . get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $inputs = array(
            'active',
            'forced',
            'create',
            'loginattribute',
        );
        foreach ($inputs as $input) {
            $this->set(
                $input,
                App()->getRequest()->getPost($input),
                'Survey',
                $surveyId
            );
        }
        if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl(
                'surveyAdministration/view',
                array('surveyid' => $surveyId)
            );
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $aLangInputs = array(
            'LdapIntroductions',
            'LdapDescriptions',
        );
        foreach ($aLangInputs as $input) {
            $postedValues = array();
            foreach ($oSurvey->getAllLanguages() as $lang) {
                $postedValues[$lang] = App()->getRequest()->getPost($input . "_" . $lang);
            }
            $this->set(
                $input,
                $postedValues,
                'Survey',
                $surveyId
            );
        }
        $redirectUrl = Yii::app()->createUrl(
            'admin/pluginhelper/sa/sidebody',
            array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId)
        );
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * see beforeSurveyPage
     */
    public function beforeControllerAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get("controller") != "survey" || $this->getEvent()->get("action") != "index") {
            return;
        }

        $surveyId = App()->getRequest()->getQuery('sid');
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            return;
        }
        if (App()->getRequest()->getParam('action') == 'previewgroup' || App()->getRequest()->getParam('action') == 'previewquestion') {
            return;
        }
        if ($oSurvey->active != "Y") {
            return;
        }
        if (!$oSurvey->getHasTokensTable()) {
            return;
        }
        $active = $this->get('active', 'Survey', $surveyId, '');
        if ($active === "") {
            $active = $this->get('active', null, null, $this->settings['active']['default']);
        }
        if (!$active) {
            return;
        }

        /* Must check if $attribute is here */
        $attribute = $this->GetUidAttribute($surveyId);
        if (empty($attribute)) {
            return;
        }
        if (
            App()->getRequest()->getParam('clearall') == 'clearall' &&
            ( App()->getRequest()->getPost('confirm-clearall') || App()->getRequest()->getParam('ldap-logout') == 'logout')
        ) {
            $this->unsetLdapToken($surveyId);
            if (App()->getRequest()->getParam('ldap-logout') == 'logout' 
                || ( $this->get('ClearAllLogoutLdapUser', null, null, 1) && $this->surveyAllowSameUser($surveyId))
            ) {
                $this->unsetCurrentLdapUser();
            }
        }
        if (App()->getRequest()->getParam('ldap-logout')) {
            $this->unsetLdapToken($surveyId);
            $this->unsetCurrentLdapUser();
        }
        if (App()->getRequest()->getParam('newtest') == 'Y') {
            $this->unsetLdapToken($surveyId);
            if ($this->get('NewtestLogoutLdapUser', null, null, 0)) {
                $this->unsetCurrentLdapUser();
            }
        }
        if (App()->getRequest()->getPost('ldap_userid') || App()->getRequest()->getPost('ldap_submit')) {
            $this->checkLdapTokenAuthentication($surveyId);
        }
        $forced = $this->get('forced', 'Survey', $surveyId, '');
        if ($forced === "") {
            $forced = $this->get('forced', null, null, $this->settings['forced']['default']);
        }
        $token = App()->getRequest()->getParam('token');
        $tokenSession = null;
        if(App()->getRequest()->getParam('newtest') != 'Y' && isset($_SESSION['survey_' . $surveyId]['token'])) {
            $tokenSession = $_SESSION['survey_' . $surveyId]['token'];
        }
        if (empty($token)) {
            $token = $tokenSession;
        }

        /* If session is already started but different than param : must delete */
        if ($tokenSession && $token != $tokenSession) {
            unset($tokenSession);
            $this->unsetLdapToken($surveyId);
            if ($this->surveyAllowSameUser($surveyId)) {
                $this->unsetCurrentLdapUser();
            }
        }
        $ldapSurveyToken = $this->getLdapToken($surveyId);
        if ($ldapSurveyToken && ($ldapSurveyToken == $token)) {
            /* OK : we are connected to this survey with this token : always valid */
            return;
        }
        $adminaccess = $this->get('adminaccess', 'Survey', $surveyId, $this->get('adminaccess', null, null, 1));
        if ($token) {
            if ($forced == "never") {
                /* No need to check */
                return;
            }
            if ($adminaccess) {
                $permission = Permission::model()->hasSurveyPermission($surveyId, 'responses', 'update') && Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'read');
                if ($permission) {
                    /* Admin access */
                    return;
                }
            }
        } elseif(defined('\reloadAnyResponse\Utilities::API')) {
            if (App()->getRequest()->getParam('srid') && \reloadAnyResponse\Utilities::getSetting($surveyId, 'allowAdminUser')) {
                if (\Permission::model()->hasSurveyPermission($surveyId, 'responses', 'update')) {
                    return;
                }
            }
        }

        if ($forced == "always" || !$token) {
            /* Start showing */
            $this->checkUserOrShowForm($surveyId);
            return;
        }

        /* we know $forced == "auto" and we have a token */
        $oToken = Token::model($surveyId)->findByToken($token);
        if (empty($oToken)) {
            return;
        }
        if (empty($oToken->$attribute)) {
            return;
        }
        /* Start showing */
        $this->checkUserOrShowForm($surveyId);
    }

    /**
     * see beforeToolsMenuRender event
     * @deprecated ? See https://bugs.limesurvey.org/view.php?id=15476
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('surveyId');
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aMenuItem = array(
            'label' => $this->gT('LDAP authentication'),
            'iconClass' => 'fa fa-user-circle',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $this->getEvent()->append('menuItems', array($menuItem));
    }

    /**
     * get translation
     * @param string $string to translate
     * @param string escape mode
     * @param string language, current by default
     * @return string
     */
    public function gT($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($string, $sEscapeMode, $sLanguage);
    }

    /**
     * unset LDAP session id
     * @param integer $surveyid
     * @return void
     */
    private function unsetLdapToken($surveyId)
    {
        $ldapSurveyTokens = App()->session['ldapSurveyTokens'];
        unset($ldapSurveyTokens[$surveyId]);
        App()->session['ldapSurveyTokens'] = $ldapSurveyTokens;
    }

    /**
     * set LDAP session id
     * @param integer $surveyid
     * @param string $uid
     * @return void
     */
    private function setLdapToken($surveyId, $token)
    {
        $ldapSurveyTokens = (array) App()->session['ldapSurveyTokens'];
        $ldapSurveyTokens[$surveyId] = $token;
        App()->session['ldapSurveyTokens'] = $ldapSurveyTokens;
        $_SESSION['survey_' . $surveyId]['token'] = $token;
        $_POST['token'] = $token;
    }

    /**
     * Get the url to start survey with current params
     * @param integer $surveyid
     * @param string route
     * @return string
     */
    private function getStartUrl($surveyId, $route = 'survey/index')
    {
        $params = array(
            'sid' => $surveyId
        );
        if (App()->getRequest()->getParam('lang')) {
            $params['lang'] = App()->getRequest()->getParam('lang');
        }
        if (App()->getRequest()->getQuery('newtest')) {
            $params['newtest'] = App()->getRequest()->getParam('lang');
        }
        /* Core get value never prefiled */
        $reservedGetValues = array(
            'token',
            'sid',
            'gid',
            'qid',
            'lang',
            'newtest',
            'action',
            'seed'
        );
        /* Extra get value to be removed from URL */
        $extraReservedGetValues = array(
            App()->getUrlManager()->routeVar,
            App()->request->csrfTokenName
        );
        $allReservedGetValues = array_merge($reservedGetValues, $extraReservedGetValues);
        $request = Yii::app()->getRequest();
        $getValues = array();
        if (in_array($request->getRequestType(), ['GET', 'POST'])) {
            $getValues = array_diff_key(
                $request->getQueryParams(),
                array_combine($allReservedGetValues, $allReservedGetValues)
            );
        }
        return  App()->createUrl(
            $route,
            array_merge(
                $params,
                $getValues
            )
        );
    }

    /**
     * set LDAP session id
     * @param integer $surveyid
     * @param string $uid
     * @return string|null
     */
    private function getLdapToken($surveyId)
    {
        $ldapSurveyTokens = App()->session['ldapSurveyTokens'];
        if (!empty($ldapSurveyTokens[$surveyId])) {
            return $ldapSurveyTokens[$surveyId];
        }
    }

    /**
     * Get the current attribute set
     * @param $surveyid
     * @return string|null
     */
    private function getUidAttribute($surveyId)
    {
        $attribute = $this->get('loginattribute', 'Survey', $surveyId, '');
        if ($attribute === "") {
            $attribute = $this->get("loginattribute", null, null, null);
        }
        $aBaseAttributes = array(
            'firstname',
            'lastname',
            'email',
            'participant_id',
        );
        if (in_array($attribute, $aBaseAttributes)) {
            return $attribute;
        }
        $surveyAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
        if (array_key_exists($attribute, $surveyAttributes)) {
            return $attribute;
        }
        $descriptionloginattribute = $this->get('descriptionloginattribute', null, null, '');
        if (empty($descriptionloginattribute)) {
            return null;
        }

        foreach ($surveyAttributes as $attribute => $information) {
            if (!empty($information['description']) && $information['description'] == $descriptionloginattribute) {
                return $attribute;
            }
            if (empty($information['description']) && $attribute == $descriptionloginattribute) {
                return $attribute;
            }
        }
    }

    /**
     * Check usage of already connected account
     * If : no show the login form
     */
    private function checkUserOrShowForm($surveyId)
    {
        if ($this->surveyAllowSameUser($surveyId)) {
            $ldapUser = $this->getCurrentLdapUser();
            if ($ldapUser) {
                $oToken = $this->updateOrCreateToken($surveyId, $ldapUser['ldapUserid'], $ldapUser['userInfo']);
                if (!empty($oToken->token)) {
                    $this->setLdapToken($surveyId, $oToken->token);
                    return;
                }
            }
        }
        $this->showLdapTokenForm($surveyId);
    }
    /**
     * Show the LDAP connect for current survey
     * @param integer $surveyId
     * @return void
     */
    private function showLdapTokenForm($surveyId, $aErrors = array())
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $language = App()->getLanguage();
        if (!in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        $this->subscribe('getPluginTwigPath');

        $aRenderData = array();
        /* Still usaed in getLanguageChangerDatas */
        App()->setConfig('surveyID', $surveyId);
        $renderData['aSurveyInfo'] = getSurveyInfo($surveyId, $language);
        $renderData['aSurveyInfo']['surveyUrl'] = $this->getStartUrl($surveyId);
        if ($renderData['aSurveyInfo']['additional_languages']) {
            $renderData['aSurveyInfo']['alanguageChanger']['show']  = true;
            $renderData['aSurveyInfo']['alanguageChanger']['datas'] = getLanguageChangerDatas($language);
        }
        $renderData['aSurveyInfo']['include_content'] = 'userforms';
        $renderData['aSurveyInfo']['showprogress'] = false;
        $renderData['aSurveyInfo']['aForm'] = array(
            'sType' => 'form_ldapauth',
            'aEnterErrors' => $aErrors
        );

        $renderData['LdapTokenAuthenticate'] = array(
            "introduction" => $this->get('LdapIntroduction', null, null, $this->settings['LdapIntroduction']['default']),
            "description" =>  $this->get('LdapDescription', null, null, $this->settings['LdapDescription']['default']),
            'lang' => array(
                'Use your email to register to this survey.' => $this->gT('Use your email to register to this survey.'),
            )
        );
        $renderData['LdapTokenAuthenticate']['errors'] = $aErrors;
        // Descripotuon
        $aLdapIntroductions = $this->get('LdapIntroductions', 'Survey', $surveyId, array());
        if (!empty($aLdapIntroductions[$language])) {
            $renderData['LdapTokenAuthenticate']['introduction'] = $aLdapIntroductions[$language];
        }
        $aLdapDescriptions = $this->get('LdapDescriptions', 'Survey', $surveyId, array());
        if (!empty($aLdapDescriptions[$language])) {
            $renderData['LdapTokenAuthenticate']['description'] = $aLdapDescriptions[$language];
        }
        $renderData['LdapTokenAuthenticate']['form'] = array(
            'ldapUserId' => App()->getRequest()->getParam('ldap_userid'),
            'ldapPassword' => App()->getRequest()->getPost('ldap_password'),
        );
        if ($oSurvey->getIsAllowRegister()) {
            $renderData['LdapTokenAuthenticate']['registerUrl'] = $this->getStartUrl($surveyId, 'register/index');
        }
        // User id
        $attribute = $this->getUidAttribute($surveyId);
        $labelUid =  gT("User ID");
        switch ($attribute) {
            case 'firstname':
                $labelUid = gT("First name");
                break;
            case 'lastname':
                $labelUid = gT("Last name");
                break;
            case 'email':
                $labelUid = gT("Email address");
                break;
            case 'participant_id':
                $labelUid = gT("Participant ID");
                break;
            default:
                // We have an attribute
                $oSurveyLanguage = SurveyLanguageSetting::model()->findByPk([
                    'surveyls_survey_id' => $surveyId,
                    'surveyls_language' => $language
                ]);
                if ($oSurveyLanguage && $oSurveyLanguage->surveyls_attributecaptions) {
                    $attributecaptions = @json_decode($oSurveyLanguage->surveyls_attributecaptions, 1);
                    if (!empty($attributecaptions[$attribute])) {
                        $labelUid = $attributecaptions[$attribute];
                    }
                }
        }
        $renderData['LdapTokenAuthenticate']['labelUid'] = $labelUid;
        Template::model()->getInstance(null, $surveyId);
        Yii::app()->twigRenderer->renderTemplateFromFile('layout_global.twig', $renderData, false);
    }

    /* Add the needed file */
    public function getPluginTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__) . "/twig/add";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * Check the authentication
     * @param integer $surveyId the survey id
     * @return void
     */
    private function checkLdapTokenAuthentication(int $surveyId)
    {
        $ldapUserid = App()->getRequest()->getPost('ldap_userid');
        $ldapPassword = App()->getRequest()->getPost('ldap_password');

        if (empty($ldapUserid) || empty($ldapPassword)) {
            $this->showLdapTokenForm($surveyId, array(gT('Incorrect username and/or password!')));
        }
        /* Set path of Alias at start */
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $LdapAuthenticate = new LdapTokenAuthenticate\LdapAuthentication(
            $this->getPluginSettings(true),
        );
        $user = $LdapAuthenticate->authenticate($ldapUserid, $ldapPassword);
        if (empty($user)) {
            $error = $LdapAuthenticate->ErrorInfo;
            if (empty($error)) {
                $error = $this->gt("Unable to authenticate user.");
            }
            $this->showLdapTokenForm($surveyId, array($error));
        }
        // OK we have the userid : get the related token if exist.
        $oToken = $this->updateOrCreateToken($surveyId, $ldapUserid, $user);
        if (empty($oToken)) {
            $this->showLdapTokenForm($surveyId, array(gT("We are sorry but you are not allowed to enter this survey.")));
        }
        $this->setLdapToken($surveyId, $oToken->token);
        if ($this->surveyAllowSameUser($surveyId)) {
            $this->setCurrentLdapUser($ldapUserid, $user);
        }
    }

    /**
     * Create token
     * @param integer $surveyId
     * @param string $ldapUserId
     * @param array $userInfo
     * @return \Token
     */
    private function updateOrCreateToken($surveyId, $ldapUserId, $userInfo)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $uidAttribute = $this->getUidAttribute($surveyId);
        $oCriteria = new CDbCriteria();
        $oCriteria->compare("LOWER(" . App()->db->quoteColumnName($uidAttribute) .")", strtolower($ldapUserId));
        $oToken = Token::model($surveyId)->find($oCriteria);
        if ($oToken) {
            $oToken->decrypt();
            if (empty($oToken->token)) {
                $oToken->generateToken();
            }
        } else {
            /* Check if we allow user create (register) */
            $allowCreate = $this->get('create', 'Survey', $surveyId, '');
            if ($allowCreate == "N") {
                return null;
            }
            if ($allowCreate == "" && !$oSurvey->getIsAllowRegister()) {
                return null;
            }
            $oToken = Token::create($surveyId);
            $oToken->setAttribute(
                $uidAttribute,
                $ldapUserId
            );
            $oToken->generateToken();
            $oToken->validfrom = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
            $oToken->usesleft = 1;
        }
        if (isset($userInfo['mail'])) {
            $oToken->email = $userInfo['mail'];
            unset($userInfo['mail']);
        }
        $oToken->emailstatus = "OK";
        if (isset($userInfo['firstname'])) {
            $oToken->firstname = $userInfo['firstname'];
            unset($userInfo['firstname']);
        }
        if (isset($userInfo['lastname'])) {
            $oToken->lastname = $userInfo['lastname'];
            unset($userInfo['lastname']);
        }
        $aAttributeByDescription = array();
        foreach ($oSurvey->getTokenAttributes() as $attribute => $information) {
            if ($attribute != $uidAttribute && !empty($information['description'])) {
                $aAttributeByDescription[$information['description']] = $attribute;
            }
        }
        foreach ($userInfo as $ldapAttribute => $value) {
            if (isset($aAttributeByDescription[$ldapAttribute])) {
                $oToken->setAttribute(
                    $aAttributeByDescription[$ldapAttribute],
                    $value
                );
            }
        }
        $event = new PluginEvent('LdapTokenAuthenticateUpdateOrCreateToken');
        $event->set('surveyId', $surveyId);
        $event->set('userInfo', $userInfo);
        $event->set('oToken', $oToken);
        App()->getPluginManager()->dispatchEvent($event);
        $oToken->encryptSave(false);
        return $oToken;
    }

    /**
     * Dis survey allow reusage of user
     * @parama integer $surveyId
     * @return boolean
     */
    private function surveyAllowSameUser($surveyId)
    {
        $globalKeepLdapUser = $this->get('KeepLdapUser', null, null, $this->settings['KeepLdapUser']['default']);
        $surveyLoginAttribute = $this->get('loginattribute', 'Survey', $surveyId, "");
        return $globalKeepLdapUser && $surveyLoginAttribute === "";
    }
    /**
    * set actual url when activate
    * @see parent::saveSettings()
    */
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        if (isset($settings['usersearchbase'])) {
            $usersearchbase = preg_split('/\r\n|\r|\n|;/', $settings['usersearchbase']);
            $settings['usersearchbase'] = implode("\n", $usersearchbase);
        }
        if (isset($settings['otherattribute'])) {
            $otherattribute = preg_split('/\r\n|\r|\n|;/', $settings['otherattribute']);
            $settings['otherattribute'] = implode("\n", $otherattribute);
        }
        parent::saveSettings($settings);
    }

    /**
     * Save the current LDAP user in session
     * @param string $ldapUserid
     * @param array $user
     * @return void
     */
    private function setCurrentLdapUser($ldapUserid, $userInfo)
    {
        $ldapUser = array(
            'ldapUserid' => $ldapUserid,
            'userInfo' => $userInfo
        );
        App()->session['ldapUser'] = $ldapUser;
    }

    /**
     * Get the current user connected via LDAP (if allowedf
     * @return array|null
     */
    private function getCurrentLdapUser()
    {
        return App()->session['ldapUser'];
    }

    /**
     * Get the current user connected via LDAP (if allowedf
     * @return array|null
     */
    private function unsetCurrentLdapUser()
    {
        App()->session['ldapUser'] = null;
    }
}
