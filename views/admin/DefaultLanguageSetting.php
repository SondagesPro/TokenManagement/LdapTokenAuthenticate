<div class="row">
    <div class="default control-label col-sm-6">
        <?php echo $lang['Default introduction']; ?>
    </div>
    <div class="col-sm-6 controls">
        <div><?php echo $sLdapDefault['introduction']; ?></div>
    </div>
</div>
<div class="row">
    <div class="default control-label col-sm-6">
        <?php echo $lang['Default description']; ?>
    </div>
    <div class="col-sm-6 controls">
        <div class="well">
            <?php echo viewHelper::purified($sLdapDefault['description']); ?>
        </div>
    </div>
</div>
