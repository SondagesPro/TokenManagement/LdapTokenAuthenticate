<div class="form-group">
    <label class="default control-label col-sm-6" for="<?php echo CHtml::getIdByName($name); ?>">
        <?php printf($lang['Introduction text in %s (%s)'],$languageDetail['description'],$languagecode); ?>
    </label>
    <div class="col-sm-6 controls">
            <?php
                echo CHtml::textField($name, $value,
                    array("class"=>"form-control", 'id' => $name, 'placeholder' => $placeholder));
            ?>
        </div>
    </div>
</div>
